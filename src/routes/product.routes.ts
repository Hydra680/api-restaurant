import { Router } from "express";
import { TokenValidator } from "../libs/verifyToken";
import { createProduct, getProductIngredient, getProducts }  from '../controllers/controller.product'
const router:Router = Router();

router.get("/all",TokenValidator,getProducts);
router.get("/:productId",TokenValidator,getProductIngredient);
router.post("/create",TokenValidator,createProduct);

export default router;
