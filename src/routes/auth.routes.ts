import { Router } from "express";
import { login,getProfile,updateProfile, sigin } from "../controllers/controller.auth";
import { TokenValidator } from "../libs/verifyToken";
const router:Router=Router();
router.post("/login",login);
router.post("/sigin",sigin);
router.get("/profile/:nick",TokenValidator,getProfile);
router.post("/profile/:nick",TokenValidator,updateProfile);


export default router;
