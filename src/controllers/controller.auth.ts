import { Request, Response } from "express";
import { connect } from "../dataBase";
import jwt from "jsonwebtoken";
import { Usuario } from "../interface/usuario";

export async function login (req:Request,res:Response): Promise<Response> {
    const {nick,clave}=req.body;
    //console.log(nick,clave);
    const conn = await connect();
    try{
        let result:any;
        if(nick.includes("A")===true){
            result = await conn.query("SELECT * FROM administrador WHERE nick = ? AND clave = ?",[nick,clave]);
        }
        if(nick.includes("U")===true){
            result = await conn.query("SELECT * FROM usuario WHERE nick=? AND clave=?",[nick,clave])
        }  
        const usuario:Usuario =result[0][0];
        conn.end();
        //console.log(usuario);
        if(usuario){
            const token = jwt.sign({id:usuario.ci,rol:usuario.nick},process.env.SECRET_KEY || "TOkEN_TESTING");
            return res.status(200).json({message:"Bienvenido",token});
        }else{
            return res.status(400).json({message:"Usuario o contraseña incorrectos"});
        }
    }catch(e){
        console.log("error en la consulta");
        return res.status(500).json("internal server error");
    }    

}

// get profile
export async function getProfile(req:Request,res:Response): Promise<Response> {
    //parametros de la url nick
    const {nick}=req.params;
    const {rol} = req.body;
    // console.log(rol);  
    const conn = await connect();
    try {
        let result:any;
        if(nick.includes("A")===true){
            result = await conn.query("SELECT * FROM administrador WHERE nick = ?",[nick]);
        }
        if(nick.includes("U")===true){
            result = await conn.query("SELECT * FROM usuario where nick=?",[nick]);
        }
        conn.end();
        return res.status(200).json(result[0][0]);
    } catch (error) {
        console.log("error en la consulta");
    }
    return res.status(404).json("profile not found");
}

// update profile
export async function updateProfile(req:Request,res:Response): Promise<Response> {
    const {nick} = req.params;
    const usuario:Usuario = req.body;
    const conn = await connect();
    console.log(usuario);
    
    try {
        const result:any = await conn.query(
            "UPDATE usuario SET ci=?, apMat=?,apPat=?, nacionalidad=?, nombres=?, nick=?, clave=? WHERE nick=?",
            [usuario.ci, usuario.apMat,usuario.apPat,usuario.nacionalidad,usuario.nombres,nick,usuario.clave,nick]);
        conn.end();
        return res.status(200).json("profile updated");
    } catch (error) {
        console.log("error en la consulta");
        
    }
    return res.status(404).json("profile not found");
}
export async function updateProfileAdmin(req:Request,res:Response): Promise<Response> {
    const {nick} = req.params;
    const usuario:Usuario = req.body;
    const conn = await connect();
    console.log(usuario);
    
    try {
        const result:any = await conn.query(
            "UPDATE administrador SET ci=?, apMat=?,apPat=?, nacionalidad=?, nombres=?, nick=?, clave=? WHERE nick=?",
            [usuario.ci, usuario.apMat,usuario.apPat,usuario.nacionalidad,usuario.nombres,nick,usuario.clave,nick]);
        conn.end();
        return res.status(200).json("profile updated");
    } catch (error) {
        console.log("error en la consulta");
        
    }
    return res.status(404).json("profile not found");
}

export async function sigin(req:Request,res:Response): Promise<Response> {
    const usuario:Usuario = req.body;
    const conn = await connect();
    //console.log(usuario);
    try {
        const result:any = await 
        conn.query
        ("INSERT INTO usuario (ci,apMat,apPat,nacionalidad,nombres,nick,clave,id_admin) VALUES (?,?,?,?,?,?,?,?)",
        [usuario.ci,usuario.apMat,usuario.apPat,usuario.nacionalidad,usuario.nombres,usuario.nick,usuario.clave,usuario.id_admin]);
        conn.end();
        //console.log(result);
        return res.status(200).json("usuario registrado");
    } catch (error) {
        //console.log(error);
        console.log("error en la consulta");
    }
    return res.status(404).json("usuario no registrado");
}