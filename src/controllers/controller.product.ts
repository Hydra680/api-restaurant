import { Request, Response } from "express";
import { Product } from "interface/product";
import { connect } from "../dataBase";
export const getProducts = async (req: Request, res: Response) => {
    const connection = await connect();
    try {
        const products = await connection
                .query("SELECT * FROM producto WHERE descripcion != 'Refresco'");
        const bebidad = await connection.query("SELECT * FROM bebida");
        
        res.status(200).json(
            {
                Platos:products[0],
                Bebidas:bebidad[0]
            });
        
    } catch (error) {
        console.log(error);
        
        res.status(400).json("error");
    }
}

export async function getProductIngredient(req:Request,res:Response): Promise<Response> {
    //recibir parametro de la url
    const id = req.params.productId;
    const conn = await connect();
    try {
        //const result = await conn.query("SELECT p.*,i.descripcion as ingrediente FROM producto p, ingrediente i,contiene c WHERE (p.id_producto = ? and p.id_producto = c.id_producto) and i.id_ingrediente=c.id_ingrediente ORDER by p.id_producto;",[id]);
        const result = await conn.query("SELECT p.nombre, i.descripcion as ingrediente FROM producto p, ingrediente i,contiene c WHERE (p.id_producto = ? and p.id_producto = c.id_producto) and i.id_ingrediente=c.id_ingrediente ORDER by p.id_producto;",[id]);
        const products = result;
        //console.log(products);
        if(products.length>0){
            return res.status(200).json(products[0]);
        }
        return res.status(404).json("products not found");
    } catch (error) {
        console.log(error);
        return res.status(500).json("internal server error");
    }
}
// insert de producto con atributos nombre descripcion precio imagen
export async function createProduct(req:Request,res:Response): Promise<Response> {
    const newProduct:Product = req.body;
    const rol = req.body.rol;
    //console.log(rol);
    if(rol == "administrador"){
        const conn = await connect();
        try {
            //random id
            newProduct.id_producto = Math.floor(Math.random() * 1000);
            const result = await conn
            .query("INSERT INTO producto SET id_producto = ?, nombre = ?, descripcion = ?, precio = ?, imagen = ?",
            [newProduct.id_producto ,newProduct.nombre,newProduct.descripcion,newProduct.precio,newProduct.imagen]);
            return res.status(200).json({message:"product created",Product: newProduct});
        } catch (error) {
            console.log(error);
            return res.status(500).json("internal server error");
        }
    }else{
        return res.status(401).json("unauthorized");
    }
}