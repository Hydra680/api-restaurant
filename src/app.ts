import { Application } from "express";
import express from "express";
import morgan from "morgan";
import product from "./routes/product.routes";
import auth from "./routes/auth.routes";

export class App {
    app: Application;

    constructor(private port?: number | string) {
        this.app = express();
        this.settings();
        this.middlewares();
        this.routes();
        
    }

    settings() {
        this.app.set("port", this.port || process.env.PORT || 3000);
    }

    middlewares() {
        this.app.use(morgan("dev"));
        this.app.use(express.json());
    }

    routes() {
        this.app.use('/auth',auth);
        this.app.use('/product',product);
    }

    async listen() {
        await this.app.listen(this.app.get("port"));
        console.log("Server on port", this.app.get("port"));
    }
}
