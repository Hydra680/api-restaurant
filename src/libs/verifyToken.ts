import { Request, Response, NextFunction } from "express";
import jwt from 'jsonwebtoken';

interface IPayload{
    id:string,
    rol:string,
    iat:number,
    exp:number
}

export const TokenValidator = (req:Request, res:Response, next:NextFunction)=> {

    const token = req.header('auth-token');
    if(!token){
        return res.status(401).json('acess denied');
    }
    const payload = jwt.verify(token, "TOkEN_TESTING") as IPayload;
    //verificar si una subcadena existe dentro de otra cadena
    /*
        UL- ..... Es cliente
        UC- ... Cajero
        UM-.. camarero
        UF-...chef
    */
    //console.log(payload.rol);
    
    if(payload.rol.includes("A")){
        req.body.rol = "administrador";
    }
    if(payload.rol.includes("UL")){
        req.body.rol = "cliente";
    }
    if(payload.rol.includes("UC")){
        req.body.rol = "cajero";
    }
    if(payload.rol.includes("UM")){
        req.body.rol = "camarero";
    }
    if(payload.rol.includes("UF")){
        req.body.rol = "chef";
    }
    //console.log(req.body.rol);
    
    next();
}
