export interface Usuario {
    ci: number;
    nombres: string;
    apPat: string;
    apMat: string;
    nick: string;
    clave: string;
    nacionalidad: string;
    id_admin: number;
}