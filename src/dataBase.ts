import { createPool } from "mysql2/promise";

export async function connect() {
  const connection = await createPool({
    host: "172.20.0.2",
    user: "admin",
    password: "admin",
    database: "dbrestaurant",
    connectionLimit: 10,
  });
  return connection;
}


/*import { createPool, Pool } from "mysql";

//coneccion mysql
export const pool = createPool({
    host: "192.168.0.20",
    user: "root",
    password: "Hy680dra",
    database: "bdrestaurant",
    connectionLimit: 10,

});
//verificar la coneccion
pool.getConnection((err, connection) => {
    if (err) {
        console.log("error de coneccion");
        
    }
    if (connection) connection.release();
    console.log("DB is Connected");
    return;
});


*/