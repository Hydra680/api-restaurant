-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-11-2022 a las 03:01:24
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdrestaurante281`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_admin` int(5) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apPat` varchar(30) NOT NULL,
  `apMat` varchar(30) NOT NULL,
  `nick` varchar(20) NOT NULL,
  `clave` varchar(15) NOT NULL,
  `an_expe` int(5) NOT NULL,
  `salario` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_admin`, `nombre`, `apPat`, `apMat`, `nick`, `clave`, `an_expe`, `salario`) VALUES
(1, 'Lian', 'Maceda', 'Vega', 'A-LIAN', '123', 2, 2500),
(2, 'Felipe', 'Altamirano', 'Monasterios', 'A-FEL', '123', 2, 2500),
(3, 'Diana', 'Quispe', 'Chambi', 'A-DIA', '123', 2, 2500),
(4, 'Leonel', 'Lima', 'Lima', 'A-LEO', '123', 2, 2500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bebida`
--

CREATE TABLE `bebida` (
  `id_bebida` int(11) NOT NULL,
  `tipo_bedida` varchar(20) DEFAULT NULL,
  `id_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `bebida`
--

INSERT INTO `bebida` (`id_bebida`, `tipo_bedida`, `id_producto`) VALUES
(1, 'Coca cola', 1),
(2, 'Pepsi', 1),
(3, 'Sprite', 1),
(4, 'Fanta', 1),
(5, 'Jugo de Limon', 2),
(6, 'Jugo de Naranja', 2),
(7, 'Jugo de Piña', 2),
(8, 'Jugo de Maracuya', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajero`
--

CREATE TABLE `cajero` (
  `id_cajero` int(11) NOT NULL,
  `ci` int(11) NOT NULL,
  `años_exp` int(11) DEFAULT NULL,
  `ultimo_lugar` varchar(20) DEFAULT NULL,
  `salario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cajero`
--

INSERT INTO `cajero` (`id_cajero`, `ci`, `años_exp`, `ultimo_lugar`, `salario`) VALUES
(1001, 6023428, 2, 'Rest Mexican food', 2500),
(1002, 6732412, 3, 'Rest Campestre', 2500),
(1003, 6743291, 2, 'Rest Buen Gusto', 2500),
(1004, 6853618, 2, 'Rest La Taverna', 2500),
(1005, 6875432, 2, 'Rest American', 2500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `camarero`
--

CREATE TABLE `camarero` (
  `id_camarero` int(11) NOT NULL,
  `años_exp` int(11) DEFAULT NULL,
  `ultimo_lugar` varchar(20) DEFAULT NULL,
  `salario` int(11) DEFAULT NULL,
  `ci` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `camarero`
--

INSERT INTO `camarero` (`id_camarero`, `años_exp`, `ultimo_lugar`, `salario`, `ci`) VALUES
(2001, 2, 'Rest Caballeriza', 1800, 7023012),
(2002, 3, 'Rest Casa del Camba', 1800, 7263811),
(2003, 2, 'Rest El arriero', 1800, 7345677),
(2004, 2, 'Rest TIA LIA', 1800, 7635422),
(2005, 3, 'Rest Lomitos', 1800, 7654893),
(2006, 2, 'Rest El tren Rojo', 1800, 7833427),
(2007, 2, 'Rest Subway', 1800, 7874334),
(2008, 2, 'Rest La Llave', 1800, 7896685);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chef`
--

CREATE TABLE `chef` (
  `id_chef` int(11) NOT NULL,
  `años_exp` int(11) DEFAULT NULL,
  `ultimo_lugar` varchar(20) DEFAULT NULL,
  `salario` int(11) DEFAULT NULL,
  `ci` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `chef`
--

INSERT INTO `chef` (`id_chef`, `años_exp`, `ultimo_lugar`, `salario`, `ci`) VALUES
(5001, 4, 'Rest Fogon', 3500, 8536188),
(5002, 5, 'Rest El Valle', 3800, 8998765);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `hora_ingreso` date DEFAULT current_timestamp(),
  `ci` int(11) NOT NULL,
  `id_cajero` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `hora_ingreso`, `ci`, `id_cajero`) VALUES
(1001, '2022-10-11', 2159951, 1005),
(1002, '2022-10-11', 2322189, 1005),
(1003, '2022-10-11', 2333421, 1005),
(1004, '2022-10-11', 2398451, 1005),
(1005, '2022-10-11', 2544309, 1005),
(1006, '2022-10-11', 2653878, 1003),
(1007, '2022-10-11', 2674355, 1003),
(1008, '2022-10-11', 2677380, 1003),
(1009, '2022-10-11', 2782375, 1003),
(1010, '2022-10-11', 2876332, 1003),
(1011, '2022-10-11', 3522563, 1002),
(1012, '2022-10-11', 3578990, 1002),
(1013, '2022-10-11', 3623418, 1002),
(1014, '2022-10-11', 3654377, 1002),
(1015, '2022-10-11', 3874947, 1002),
(1016, '2022-10-11', 4423128, 1004),
(1017, '2022-10-11', 4532622, 1004),
(1018, '2022-10-11', 4533721, 1004),
(1019, '2022-10-11', 5623418, 1004),
(1020, '2022-10-11', 5678342, 1004),
(1021, '2022-10-11', 5784322, 1001);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_cliente`
--

CREATE TABLE `contacto_cliente` (
  `id_contacto` int(11) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Telefono` int(10) NOT NULL,
  `Mensaje` text NOT NULL,
  `Contacto` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `contacto_cliente`
--

INSERT INTO `contacto_cliente` (`id_contacto`, `Nombre`, `Email`, `Telefono`, `Mensaje`, `Contacto`) VALUES
(1, 'Daniel Velarde', 'dvq@gmail.com', 65570845, 'Hola probando', 'telefono');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contiene`
--

CREATE TABLE `contiene` (
  `id_producto` int(11) NOT NULL,
  `id_ingrediente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `contiene`
--

INSERT INTO `contiene` (`id_producto`, `id_ingrediente`) VALUES
(16, 1),
(3, 4),
(2, 35),
(13, 9),
(12, 18),
(7, 2),
(11, 23),
(6, 11),
(15, 3),
(8, 37),
(10, 6),
(9, 30),
(4, 7),
(18, 13),
(17, 29),
(5, 16),
(2, 15),
(14, 25),
(1, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_producto`
--

CREATE TABLE `detalle_producto` (
  `codproducto` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `detalle_producto`
--

INSERT INTO `detalle_producto` (`codproducto`, `iduser`, `cantidad`, `precio`) VALUES
(1, 784845, 3, 13),
(2, 784845, 2, 40),
(12, 784845, 1, 38);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eli_factura`
--

CREATE TABLE `eli_factura` (
  `id_cajero` int(11) NOT NULL,
  `id_factura` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `eli_factura`
--

INSERT INTO `eli_factura` (`id_cajero`, `id_factura`, `total`) VALUES
(1, 1, 62),
(1, 2, 35),
(1, 3, 20),
(1, 4, 40),
(1, 5, 40),
(1, 6, 62),
(1, 1, 62),
(1, 2, 35),
(1, 3, 20),
(1, 4, 40);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id_factura` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_cajero` int(11) DEFAULT NULL,
  `total_pagar` int(11) DEFAULT NULL,
  `efectivo` int(11) DEFAULT NULL,
  `cambio` int(11) DEFAULT NULL,
  `fecha` date DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id_factura`, `id_cliente`, `id_cajero`, `total_pagar`, `efectivo`, `cambio`, `fecha`) VALUES
(80001, 1001, 1001, 70, 100, 30, '2022-10-11'),
(80002, 1002, 1001, 45, 50, 5, '2022-10-11'),
(80003, 1003, 1001, 60, 70, 10, '2022-10-11'),
(80004, 1004, 1001, 35, 50, 15, '2022-10-11'),
(80005, 1005, 1001, 85, 100, 15, '2022-10-11'),
(80006, 1006, 1002, 50, 50, NULL, '2022-10-11'),
(80007, 1007, 1002, 55, 60, 5, '2022-10-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingrediente`
--

CREATE TABLE `ingrediente` (
  `id_ingrediente` int(11) NOT NULL,
  `precio` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `id_chef` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ingrediente`
--

INSERT INTO `ingrediente` (`id_ingrediente`, `precio`, `descripcion`, `cantidad`, `id_chef`) VALUES
(1, 14, 'Carne de Pollo', 30, 5001),
(2, 28, 'Carne Cerdo', 30, 5001),
(3, 35, 'Carne Res', 30, 5001),
(4, 35, 'Carne de Llama', 15, 5001),
(5, 32, 'Queso', 15, 5001),
(6, 6, 'Cebolla', 30, 5001),
(7, 6, 'Tomate', 50, 5001),
(8, 10, 'Morron', 30, 5001),
(9, 10, 'Arroz', 3, 5001),
(10, 9, 'Fideo', 20, 5001),
(11, 7, 'Papa', 20, 5002),
(12, 14, 'Aceite', 35, 5002),
(13, 5, 'Harina', 15, 5002),
(15, 5, 'Azucar', 10, 5002),
(16, 2, 'Sal', 15, 5002),
(17, 10, 'Lechuga', 10, 5002),
(18, 10, 'Acelga', 25, 5002),
(19, 10, 'Arveja', 15, 5002),
(20, 10, 'Coliflor', 15, 5002),
(21, 5, 'Brocoli', 15, 5001),
(22, 6, 'Haba', 5, 5001),
(23, 7, 'Locoto', 9, 5001),
(24, 14, 'Choclo', 15, 5001),
(25, 14, 'Mani', 7, 5001),
(26, 5, 'Repollo', 5, 5001),
(27, 10, 'Vainita', 9, 5001),
(28, 20, 'Zanahoria', 3, 5001),
(29, 16, 'Salchicha', 25, 5001),
(30, 35, 'Aji', 5, 5001),
(31, 1, 'Pan', 50, 5002),
(32, 6, 'Leche', 15, 5002),
(33, 20, 'Limon', 60, 5002),
(34, 12, 'Naranja', 40, 5001),
(35, 10, 'Piña', 20, 5001),
(36, 14, 'Maracuya', 40, 5001),
(37, 1, 'Huevo', 80, 5001);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE `mesa` (
  `id_mesa` int(11) NOT NULL,
  `capacidad` int(11) DEFAULT NULL,
  `id_camarero` int(11) DEFAULT NULL,
  `disponible` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mesa`
--

INSERT INTO `mesa` (`id_mesa`, `capacidad`, `id_camarero`, `disponible`) VALUES
(1, 4, 2001, 'si'),
(2, 6, 2002, 'si'),
(3, 4, 2003, 'si'),
(4, 6, 2004, 'si'),
(5, 4, 2005, 'si'),
(6, 6, 2006, 'si'),
(7, 4, 2007, 'si'),
(8, 6, 2008, 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden`
--

CREATE TABLE `orden` (
  `id_orden` int(11) NOT NULL,
  `id_mesa` int(11) DEFAULT NULL,
  `id_chef` int(11) DEFAULT NULL,
  `fecha` date DEFAULT current_timestamp(),
  `id_factura` int(11) DEFAULT NULL,
  `tipo_pago` varchar(20) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `habilita` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `orden`
--

INSERT INTO `orden` (`id_orden`, `id_mesa`, `id_chef`, `fecha`, `id_factura`, `tipo_pago`, `id_cliente`, `habilita`) VALUES
(10001, 1, 5001, '2022-10-11', 80001, 'efectivo', 1001, 'si'),
(10002, 2, 5001, '2022-10-11', 80002, 'efectivo', 1002, 'si'),
(10003, 3, 5001, '2022-10-11', 80003, 'efectivo', 1003, 'si'),
(10004, 4, 5001, '2022-10-11', 80004, 'efectivo', 1004, 'si'),
(10005, 5, 5002, '2022-10-11', 80005, 'efectivo', 1005, 'si'),
(10006, 6, 5002, '2022-10-11', 80006, 'efectivo', 1006, 'si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_producto`
--

CREATE TABLE `orden_producto` (
  `id_orden` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plato`
--

CREATE TABLE `plato` (
  `id_plato` int(11) NOT NULL,
  `tipo_plato` varchar(20) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `plato`
--

INSERT INTO `plato` (`id_plato`, `tipo_plato`, `id_producto`) VALUES
(1, 'Plato Especial', 3),
(2, 'Plato Especial', 4),
(3, 'Plato Especial', 5),
(4, 'Plato Especial', 6),
(5, 'Plato Especial', 7),
(6, 'Plato Especial', 8),
(7, 'Plato Especial', 9),
(8, 'Plato Especial', 10),
(9, 'Sopas', 11),
(10, 'Sopas', 12),
(11, 'Sopas', 13),
(12, 'Sopas', 14),
(13, 'Plato Extra', 15),
(14, 'Plato Extra', 16),
(15, 'Plato Extra', 17),
(16, 'Plato Extra', 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre`, `descripcion`, `precio`, `imagen`) VALUES
(1, 'Gaseosa', 'Refresco', 20, 'refrescos'),
(2, 'Jugo', 'Refresco', 15, 'jugos_fruta'),
(3, 'Charquekan', 'Especial', 40, 'charque'),
(4, 'Pique Macho', 'Especial', 40, 'pique_macho'),
(5, 'Silpancho', 'Especial', 30, 'silpancho'),
(6, 'Fritanga', 'Especial', 30, 'fritanga\r\n'),
(7, 'Chicharron', 'Especial', 30, 'chicharron_cerdo'),
(8, 'Majadito', 'Especial', 25, 'majadito'),
(9, 'Picante mixto', 'Especial', 40, 'picante_mixto'),
(10, 'Milanesa de Pollo', 'Especial', 40, 'milanesa_pollo'),
(11, 'Fricase', 'Sopas', 35, 'fricase'),
(12, 'Chairo', 'Sopas', 35, 'chairo_paceño'),
(13, 'Caldo de pollo', 'Sopas', 15, 'caldo_pollo'),
(14, 'Sopa de mani', 'Sopas', 18, 'sopa_mani'),
(15, 'Hamburguesa', 'Extras', 25, 'hamburguesa_messi'),
(16, 'Alitas de Pollo', 'Extras', 38, 'alitas_pollo'),
(17, 'Salchipapa', 'Extras', 20, 'salchipapa'),
(18, 'Pollo Broaster', 'Extras', 25, 'pollo-broaster\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `res_pro_orden`
--

CREATE TABLE `res_pro_orden` (
  `correlativo` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `nombre_pro` varchar(60) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `fecha_eli` date DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `res_pro_orden`
--

INSERT INTO `res_pro_orden` (`correlativo`, `id_producto`, `nombre_pro`, `precio`, `cantidad`, `fecha_eli`) VALUES
(1, 4, 'Milanesa de pollito', 40, 10, '2022-10-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `ci` int(11) NOT NULL,
  `nombres` varchar(30) DEFAULT NULL,
  `apPat` varchar(30) DEFAULT NULL,
  `apMat` varchar(30) DEFAULT NULL,
  `nick` varchar(20) DEFAULT NULL,
  `clave` varchar(20) DEFAULT NULL,
  `nacionalidad` varchar(30) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`ci`, `nombres`, `apPat`, `apMat`, `nick`, `clave`, `nacionalidad`, `id_admin`) VALUES
(2159951, 'Laura', 'Velarde', 'Llanos', 'UL-LAURA', '2019', 'Peru', 1),
(2322189, 'Pedro', 'Ramirez', 'Martinez', 'UL-PEDRO', '2017', 'Bolivia', 1),
(2333421, 'Carlos', 'Peredo', 'Aguilar', 'UL-CARLOS', '2006', 'Bolivia', 3),
(2398451, 'Itati', 'Mendez', 'Torrez', 'UL-ITATI', '2007', 'Argentina', 3),
(2544309, 'Rosa', 'Chamaca', 'Gonzales', 'UL-ROSA', '2020', 'Bolivia', 1),
(2653878, 'Osvaldo', 'Tarqui', 'Choque', 'UL-OSVALDO', '2009', 'Bolivia', 3),
(2674355, 'Wara ', 'Balanza ', 'Perez', 'UL-WARA', '2015', 'Chile', 1),
(2677380, 'Rocio', 'Altamirano', 'Chamaca', 'UL-ROCIO', '2018', 'Bolivia', 1),
(2782375, 'Gabriela', 'Salas', 'Guzman', 'UL-GAB', '2016', 'Bolivia', 1),
(2876332, 'Hugo', 'Salas', 'Valencia', 'UL-HUGO', '2008', 'Bolivia', 3),
(3522563, 'Pablo', 'Silva', 'MamanI', 'UL-PABLO', '2010', 'Argentina', 3),
(3578990, 'Giselle', 'Flores', 'Villamonte', 'UL-GISELLE', '2014', 'Bolivia', 3),
(3623418, 'Manuel ', 'Cervantes', 'Daza', 'UL-MANUEL', '2011', 'Argentina', 3),
(3654377, 'Soledad', 'Saldaña', 'Paca', 'UL-SOLEDAD', '2012', 'Bolivia', 3),
(3874947, 'Antonio ', 'Moya', 'Deheza', 'UL-ANTONIO', '2013', 'Bolivia', 3),
(4423128, 'Nelson', 'Condori', 'Rodriguez', 'UL-NELSON', '2000', 'Bolivia', 3),
(4532622, 'Angel', 'Rojas', 'Garcia', 'UL-ANGEL', '2004', 'Bolivia', 3),
(4533721, 'Virginia', 'Chura', 'Acosta', 'UL-VIRGINIA', '2002', 'Bolivia', 3),
(5623418, 'Francisco', 'Camacho', 'Torrez', 'UL-FRAN', '2001', 'Bolivia', 3),
(5678342, 'Pamela', 'Gonzales', 'Cruz', 'UL-PAMELA', '2003', 'Bolivia', 3),
(5784322, 'Gabriela', 'Acarapi', 'Paco', 'UL-GABRIELA', '2005', 'Bolivia', 3),
(6023428, 'Lupe', 'Perez', 'Cossio', 'UC-LUPE', '3003', 'Bolivia', 1),
(6732412, 'Fernando', 'Mercado', 'Condori', 'UC-FER', '3002', 'Bolivia', 3),
(6743291, 'Daniela', 'Ramirez', 'Cavinas', 'UC-DANI', '3001', 'Bolivia', 3),
(6853618, 'Leo ', 'Lima', 'Lima', 'UC-LEO', '3004', 'Bolivia', 1),
(6875432, 'Jorge', 'Poma', 'Laura', 'UC-JORGE', '3000', 'Bolivia', 3),
(7023012, 'Tomas', 'Nava', 'Loayza', 'UM-TOMAS', '4006', 'Bolivia', 3),
(7263811, 'Emanuel', 'Oblitas', 'Quiroga ', 'UM-EMAN', '4000', 'Bolivia', 1),
(7345677, 'Jimena', 'Leaño', 'Terceros', 'UM-JIM', '4007', 'Bolivia', 3),
(7635422, 'Carolina', 'Chambi', 'Vega', 'UM-CAROLINA', '4001', 'Peru', 1),
(7654893, 'Henry', 'Ticona', 'Jaramillo', 'UM-HENRY', '4005', 'Bolivia', 3),
(7833427, 'Carla', 'Vargas', 'Vega', 'UM-CARLA', '4003', 'Bolivia', 1),
(7874334, 'Mario', 'Mejia', 'Torres', 'UM-MARIO', '3005', 'Bolivia', 3),
(7896685, 'Veronica', 'Lopez', 'Mamani', 'UM-VERO', '4004', 'Bolivia', 3),
(8536188, 'Ignacio', 'Crespo', 'Nava', 'UF-IGNACIO', '5000', 'Bolivia', 3),
(8998765, 'Josselin', 'Molina', 'Vaca', 'UF-JOSS', '5001', 'Bolivia', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_eliminado`
--

CREATE TABLE `usuario_eliminado` (
  `correlativo` int(10) NOT NULL,
  `NombreCompleto` varchar(150) NOT NULL,
  `ci` int(10) NOT NULL,
  `tipo` varchar(150) NOT NULL,
  `identificador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario_eliminado`
--

INSERT INTO `usuario_eliminado` (`correlativo`, `NombreCompleto`, `ci`, `tipo`, `identificador`) VALUES
(1, 'Pepe Martinez Lopez', 454545, '0', 1),
(2, 'Rosa Maceda Vega', 445566, 'cajero', 1),
(3, 'Nani Vallejos Vallejos', 11, 'cajero', 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `bebida`
--
ALTER TABLE `bebida`
  ADD PRIMARY KEY (`id_bebida`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `cajero`
--
ALTER TABLE `cajero`
  ADD PRIMARY KEY (`id_cajero`),
  ADD KEY `ci` (`ci`);

--
-- Indices de la tabla `camarero`
--
ALTER TABLE `camarero`
  ADD PRIMARY KEY (`id_camarero`),
  ADD KEY `ci` (`ci`);

--
-- Indices de la tabla `chef`
--
ALTER TABLE `chef`
  ADD PRIMARY KEY (`id_chef`),
  ADD KEY `ci` (`ci`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`),
  ADD KEY `ci` (`ci`),
  ADD KEY `id_cajero` (`id_cajero`);

--
-- Indices de la tabla `contacto_cliente`
--
ALTER TABLE `contacto_cliente`
  ADD PRIMARY KEY (`id_contacto`);

--
-- Indices de la tabla `contiene`
--
ALTER TABLE `contiene`
  ADD KEY `id_producto` (`id_producto`),
  ADD KEY `id_ingrediente` (`id_ingrediente`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id_factura`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_cajero` (`id_cajero`);

--
-- Indices de la tabla `ingrediente`
--
ALTER TABLE `ingrediente`
  ADD PRIMARY KEY (`id_ingrediente`),
  ADD KEY `id_chef` (`id_chef`);

--
-- Indices de la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`id_mesa`),
  ADD KEY `id_camarero` (`id_camarero`);

--
-- Indices de la tabla `orden`
--
ALTER TABLE `orden`
  ADD PRIMARY KEY (`id_orden`),
  ADD KEY `id_mesa` (`id_mesa`),
  ADD KEY `id_chef` (`id_chef`),
  ADD KEY `id_factura` (`id_factura`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Indices de la tabla `orden_producto`
--
ALTER TABLE `orden_producto`
  ADD KEY `id_orden` (`id_orden`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `plato`
--
ALTER TABLE `plato`
  ADD PRIMARY KEY (`id_plato`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `res_pro_orden`
--
ALTER TABLE `res_pro_orden`
  ADD PRIMARY KEY (`correlativo`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`ci`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indices de la tabla `usuario_eliminado`
--
ALTER TABLE `usuario_eliminado`
  ADD PRIMARY KEY (`correlativo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id_admin` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `bebida`
--
ALTER TABLE `bebida`
  MODIFY `id_bebida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `camarero`
--
ALTER TABLE `camarero`
  MODIFY `id_camarero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2009;

--
-- AUTO_INCREMENT de la tabla `chef`
--
ALTER TABLE `chef`
  MODIFY `id_chef` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5003;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1022;

--
-- AUTO_INCREMENT de la tabla `contacto_cliente`
--
ALTER TABLE `contacto_cliente`
  MODIFY `id_contacto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80008;

--
-- AUTO_INCREMENT de la tabla `ingrediente`
--
ALTER TABLE `ingrediente`
  MODIFY `id_ingrediente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `mesa`
--
ALTER TABLE `mesa`
  MODIFY `id_mesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `orden`
--
ALTER TABLE `orden`
  MODIFY `id_orden` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10008;

--
-- AUTO_INCREMENT de la tabla `plato`
--
ALTER TABLE `plato`
  MODIFY `id_plato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `res_pro_orden`
--
ALTER TABLE `res_pro_orden`
  MODIFY `correlativo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuario_eliminado`
--
ALTER TABLE `usuario_eliminado`
  MODIFY `correlativo` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bebida`
--
ALTER TABLE `bebida`
  ADD CONSTRAINT `bebida_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Filtros para la tabla `cajero`
--
ALTER TABLE `cajero`
  ADD CONSTRAINT `cajero_ibfk_1` FOREIGN KEY (`ci`) REFERENCES `usuario` (`ci`);

--
-- Filtros para la tabla `camarero`
--
ALTER TABLE `camarero`
  ADD CONSTRAINT `camarero_ibfk_1` FOREIGN KEY (`ci`) REFERENCES `usuario` (`ci`);

--
-- Filtros para la tabla `chef`
--
ALTER TABLE `chef`
  ADD CONSTRAINT `chef_ibfk_1` FOREIGN KEY (`ci`) REFERENCES `usuario` (`ci`);

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`ci`) REFERENCES `usuario` (`ci`),
  ADD CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`id_cajero`) REFERENCES `cajero` (`id_cajero`);

--
-- Filtros para la tabla `contiene`
--
ALTER TABLE `contiene`
  ADD CONSTRAINT `contiene_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `contiene_ibfk_2` FOREIGN KEY (`id_ingrediente`) REFERENCES `ingrediente` (`id_ingrediente`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`),
  ADD CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`id_cajero`) REFERENCES `cajero` (`id_cajero`);

--
-- Filtros para la tabla `ingrediente`
--
ALTER TABLE `ingrediente`
  ADD CONSTRAINT `ingrediente_ibfk_1` FOREIGN KEY (`id_chef`) REFERENCES `chef` (`id_chef`);

--
-- Filtros para la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD CONSTRAINT `mesa_ibfk_1` FOREIGN KEY (`id_camarero`) REFERENCES `camarero` (`id_camarero`);

--
-- Filtros para la tabla `orden`
--
ALTER TABLE `orden`
  ADD CONSTRAINT `orden_ibfk_1` FOREIGN KEY (`id_mesa`) REFERENCES `mesa` (`id_mesa`),
  ADD CONSTRAINT `orden_ibfk_2` FOREIGN KEY (`id_chef`) REFERENCES `chef` (`id_chef`),
  ADD CONSTRAINT `orden_ibfk_3` FOREIGN KEY (`id_factura`) REFERENCES `factura` (`id_factura`),
  ADD CONSTRAINT `orden_ibfk_4` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`);

--
-- Filtros para la tabla `orden_producto`
--
ALTER TABLE `orden_producto`
  ADD CONSTRAINT `orden_producto_ibfk_1` FOREIGN KEY (`id_orden`) REFERENCES `orden` (`id_orden`),
  ADD CONSTRAINT `orden_producto_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Filtros para la tabla `plato`
--
ALTER TABLE `plato`
  ADD CONSTRAINT `plato_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `administrador` (`id_admin`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
